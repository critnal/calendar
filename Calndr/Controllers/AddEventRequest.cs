﻿using System;

namespace Calndr.Controllers
{
    public class AddEventRequest
    {
        public Guid CalendarId { get; set; }
        public string Title { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
    }
}