﻿using System;
using System.Collections.Generic;

namespace Calndr.Controllers
{
    public class CalendarViewModel
    {
        public CalendarViewModel(Guid calendarId, IEnumerable<EventViewModel> events)
        {
            CalendarId = calendarId;
            Events = new List<EventViewModel>(events);
        }

        public Guid CalendarId { get; }
        public IReadOnlyCollection<EventViewModel> Events { get; }
    }
}