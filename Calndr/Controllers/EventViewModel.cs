﻿using System;

namespace Calndr.Controllers
{
    public class EventViewModel
    {
        public EventViewModel(Guid id, string title, string start, string end, bool canEdit)
        {
            Id = id;
            Title = title;
            Start = start;
            CanEdit = canEdit;
            End = end;
        }

        public Guid Id { get; }
        public string Title { get; }
        public string Start { get; }
        public string End { get; }
        public bool CanEdit { get; }
    }
}