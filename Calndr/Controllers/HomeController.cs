﻿using Calndr.Entities;
using Calndr.Extensions;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Calndr.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            var calendar = new Calendar(Guid.NewGuid());

            using (var db = new CalendarDbContext())
            {
                db.Calendars.Add(calendar);
                await db.SaveChangesAsync();
            }

            return Redirect($"/Home/Calendar?calendarId={calendar.Id}");
        }

        public async Task<ActionResult> Calendar(Guid calendarId)
        {
            using (var db = new CalendarDbContext())
            {
                var events = await db.Events
                    .Where(ev => ev.CalendarId == calendarId)
                    .ToListAsync();

                var currentUserId = CurrentUserId();

                var eventModels = events.Select(
                    ev => new EventViewModel(
                        ev.Id, 
                        ev.Title, 
                        ev.Start.IsoString(), 
                        ev.End.IsoString(), 
                        ev.UserId == currentUserId
                    )
                );

                var calendar = new CalendarViewModel(calendarId, eventModels);

                return View("~/Views/Home/Index.cshtml", calendar);
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddEvent(AddEventRequest request)
        {
            var @event = CreateEventFromRequest(request);

            using (var db = new CalendarDbContext())
            {
                db.Events.Add(@event);
                await db.SaveChangesAsync();
            }

            return Content(@event.Id.ToString());
        }

        private Event CreateEventFromRequest(AddEventRequest request)
        {
            var start = DateTimeOffset.Parse(request.Start);
            var end = DateTimeOffset.Parse(request.End);
            return new Event(Guid.NewGuid(), request.CalendarId, CurrentUserId(), request.Title, start, end);
        }

        [HttpPost]
        public async Task RemoveEvent(RemoveEventRequest request)
        {
            using (var db = new CalendarDbContext())
            {
                var @event = db.Events.Single(e => e.Id == request.EventId);
                if (@event.UserId == CurrentUserId())
                {
                    db.Events.Remove(@event);
                    await db.SaveChangesAsync();
                }
                else
                {
                    throw new Exception("User not allowed to delete that event.");
                }
            }
        }

        private Guid CurrentUserId() => Guid.Parse(Request.Cookies["CalndrUser"].Value);
    }
}