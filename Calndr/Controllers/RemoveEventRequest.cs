﻿using System;

namespace Calndr.Controllers
{
    public class RemoveEventRequest
    {
        public Guid EventId { get; set; }
    }
}