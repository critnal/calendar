﻿using System;

namespace Calndr.Entities
{
    public class Calendar
    {
        public Calendar(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}