﻿using System.Data.Entity;

namespace Calndr.Entities
{
    public class CalendarDbContext : DbContext
    {
        public CalendarDbContext() : base("CalendarConnectionString")
        {

        }

        public DbSet<Calendar> Calendars { get; set; }
        public DbSet<Event> Events { get; set; }
    }
}