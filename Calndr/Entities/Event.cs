﻿using System;

namespace Calndr.Entities
{
    public class Event
    {
        private Event() { }

        public Event(Guid id, Guid calendarId, Guid userId, string title, DateTimeOffset start, DateTimeOffset end)
        {
            Id = id;
            CalendarId = calendarId;
            UserId = userId;
            Title = title;
            Start = start;
            End = end;
        }

        public Guid Id { get; private set; }

        public Guid CalendarId { get; private set; }

        public Guid UserId { get; private set; }

        public string Title { get; private set; }

        public DateTimeOffset Start { get; private set; }

        public DateTimeOffset End { get; private set; }
    }
}