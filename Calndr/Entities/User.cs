﻿using System;

namespace Calndr.Entities
{
    public class User
    {
        public User (Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}