﻿using System;

namespace Calndr.Extensions
{
    public static class DateExtensions
    {
        public static string IsoString(this DateTimeOffset dto) => dto.ToString("o");
    }
}