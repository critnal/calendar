﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Calndr.Filters
{
    public class UserCookieGenerationFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var cookie = filterContext.HttpContext.Request.Cookies["CalndrUser"];
            if (cookie == null)
            {
                cookie = new HttpCookie("CalndrUser", Guid.NewGuid().ToString())
                {
                    Expires = DateTime.UtcNow.AddDays(10),
                };
                filterContext.HttpContext.Response.Cookies.Add(cookie);
            }

            //base.OnActionExecuting(filterContext);
        }
    }
}