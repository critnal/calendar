﻿using Calndr.Filters;
using Serilog;
using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Calndr
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GlobalFilters.Filters.Add(new UserCookieGenerationFilter());
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Log.Error(Server.GetLastError(), "Unhandled server error");
        }
    }
}
