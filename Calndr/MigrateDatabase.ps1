﻿Param(
  [Parameter(Mandatory=$true)]
  [string]$connectionString
)
 
 cd bin
.\migrate.exe Calndr.dll /connectionString="$($connectionString)" /connectionProviderName="System.Data.SqlClient"
cd ..