namespace Calndr.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEventName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "Name", c => c.String(nullable: false, maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "Name");
        }
    }
}
