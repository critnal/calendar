namespace Calndr.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEventStartEndDates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "Start", c => c.DateTimeOffset(nullable: false, precision: 7));
            AddColumn("dbo.Events", "End", c => c.DateTimeOffset(nullable: false, precision: 7));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "End");
            DropColumn("dbo.Events", "Start");
        }
    }
}
