namespace Calndr.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEventForeignKey : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("dbo.Events", "CalendarId", "dbo.Calendars", "Id", name: "FK_Events_Calendars");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Events", "FK_Events_Calendars");
        }
    }
}
