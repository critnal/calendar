namespace Calndr.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddUserIdToEvent : DbMigration
    {
        public override void Up()
        {
            Sql("delete dbo.Events");
            AddColumn("dbo.Events", "UserId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "UserId");
        }
    }
}
