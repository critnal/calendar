namespace Calndr.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameEventNameToTitle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "Title", c => c.String());
            DropColumn("dbo.Events", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Events", "Name", c => c.String());
            DropColumn("dbo.Events", "Title");
        }
    }
}
