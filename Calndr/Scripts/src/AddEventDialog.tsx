import * as React from "react";
import { withStyles, createStyleSheet } from 'material-ui/styles';
import Dialog, { DialogActions, DialogContent, DialogTitle } from 'material-ui/Dialog';
import Input from 'material-ui/Input/Input';
import Button from 'material-ui/Button';
import * as Util from "./Util";

const AddEventDialog: React.StatelessComponent<AddEventDialogProps> = props => (
    <Dialog
        open={props.open}
    >
        <DialogTitle>Add Event</DialogTitle>
        <Input 
            placeholder="New Event"
            onChange={(event: any) => props.onEventNameChange(event.target.value)}
        />
        <DialogActions>
            <Button color="primary" onClick={props.onClose}>Cancel</Button>
            <Button color="primary" onClick={props.onAddEvent}>Ok</Button>
        </DialogActions>
    </Dialog>
);

const styleSheet = createStyleSheet('AddEventDialog', (theme: any) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    background: theme.palette.background.paper,
  },
  dialog: {
    width: '80%',
    maxHeight: 435,
  },
}));

export default withStyles(styleSheet)(AddEventDialog);

export type AddEventDialogProps = {
    open: boolean,
    onEventNameChange: (eventName: string) => void,
    onAddEvent: () => void,
    onClose: () => void,
}