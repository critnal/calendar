import * as React from "react";
import BigCalendar from "react-big-calendar";

import * as moment from "moment";

import * as Types from "./types";

BigCalendar.momentLocalizer(moment);

const eventStyle = (event: Types.Event, start: Date, end: Date, isSelected: boolean) => ({
    style: {
        backgroundColor: event.canEdit
            ? '#3174ad'
            : 'red'
    }
});

const Calendar: React.StatelessComponent<CalendarProps> = props =>  (
    <BigCalendar
        events={props.calendar.events}
        selectable={true}
        timeslots={2}
        step={30}
        onSelectSlot={props.onSelectSlot}
        defaultView={'week'}
        onSelectEvent={props.onClickEvent}
        eventPropGetter={eventStyle}
    />
);

export default Calendar;

export type CalendarProps = {
    calendar: Types.Calendar,
    onSelectSlot: (slot: Types.ReactBigCalendar.SlotInfo) => void,
    onClickEvent: (event: Types.Event) => void
}