export type ResponseHandler = (response: Response) => void | PromiseLike<void>;

export const post = (uri: string, body: object, then: ResponseHandler): void => {
    const request = new Request(uri, {
        method: 'POST',
        body: JSON.stringify(body),
        headers: { 'Content-type': 'application/json' },
        credentials: 'same-origin'
    });
    fetch(request).then(then);
};

export const autobind = (instance: object) => {
    const proto = Object.getPrototypeOf(instance);
    const propertyNames = Object.getOwnPropertyNames(proto);
    for (const name of propertyNames) {
        const value = proto[name];
        if (typeof value === 'function') {
            (instance as any)[name] = proto[name].bind(instance);
        }
    }
};

export const isObject = (val: any) => 
        val != null && typeof val === 'object' && Array.isArray(val) === false;