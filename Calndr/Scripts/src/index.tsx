﻿import * as React from "react";
import { MuiThemeProvider } from 'material-ui/styles';
import Calendar from "./Calendar";
import AddEventDialog from "./AddEventDialog";
import * as Types from "./types";
import * as Util from "./Util";

export default class App extends React.Component<AppProps, AppProps> {
    constructor(props: AppProps) {        
        super(props);        
        this.state = props;
        
        Util.autobind(this);
    }

    render() {
        return (
            <MuiThemeProvider>
                <div>
                    <Calendar 
                        calendar={this.state.calendar} 
                        onSelectSlot={this.onSelectSlot}
                        onClickEvent={this.removeEvent}
                    />
                    <AddEventDialog 
                        open={this.state.addEvent.dialogOpen}
                        onEventNameChange={this.onAddEventDialogNameChange}
                        onAddEvent={this.onAddEvent}
                        onClose={this.onAddEventDialogClose}
                    />
                </div>
            </MuiThemeProvider>
        );
    }

    onSelectSlot(slot: Types.ReactBigCalendar.SlotInfo): void {
        const { start, end } = slot;

        this.setState({
            ...this.state,
            addEvent: {
                ...this.state.addEvent,
                dialogOpen: true,
                event: {
                    title: null,
                    start,
                    end
                }
            }
        });
    }

    onAddEventDialogNameChange(newEventName: string) {
        console.log(newEventName);
        this.setState({
            ...this.state,
            addEvent: {
                ...this.state.addEvent,
                event: {
                    ...this.state.addEvent.event,
                    title: newEventName
                }
            }
        });
    }

    onAddEvent() {
        const { title, start, end } = this.state.addEvent.event;
        
        const addEventRequest: Types.AddEventRequest = {
            calendarId: this.state.calendar.calendarId,
            title,
            start,
            end
        };

        Util.post('/Home/AddEvent', addEventRequest, response => {
            console.log(`AddEvent response.ok: ${response.ok}`);

            response.text().then(eventId => {
                const newEvent: Types.Event = {  
                    id: eventId, 
                    canEdit: true,
                    title: title,
                    start: start,
                    end: end
                };
                
                const newState: AppProps = {
                    ...this.state,
                    calendar: {
                        ...this.state.calendar,
                        events: [
                            ...this.state.calendar.events.map(e => ({ ...e })),
                            newEvent
                        ]
                    },
                    addEvent: {
                        dialogOpen: false,
                        event: null
                    }
                };

                this.setState(newState);
            });
        });
    }

    onAddEventDialogClose() {
        this.setState({
            ...this.state,
            addEvent: {
                dialogOpen: false,
                event: null
            }
        });
    }

    removeEvent(event: Types.Event): void {
        if (event.canEdit) {
            Util.post('/Home/RemoveEvent', { EventId: event.id }, response => {
                console.log(`RemoveEvent response.ok: ${response.ok}`);

                const newState = {
                    ...this.state,
                    calendar: {
                        ...this.state.calendar,
                        events: this.state.calendar.events
                            .map(e => ({ ...e }))
                            .filter(e => e.id !== event.id)
                    }
                };

                this.setState(newState);
            });
        }
    }
}

type AppProps = {
    calendar: Types.Calendar,
    addEvent: {
        dialogOpen: boolean,
        event?: {
            title: string,
            start: Date,
            end: Date
        }
    }
}