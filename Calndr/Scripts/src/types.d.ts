export type Calendar = {
    calendarId: string,
    events: Event[],
}

export type Event = {
    id: string,
    canEdit: boolean,
    title: string,
    start: Date,
    end: Date
}

export type AddEventRequest = {
    calendarId: string,
    title: string,
    start: Date,
    end: Date
}

export module ReactBigCalendar {
    export type SlotInfo = {
        start: Date, 
        end: Date, 
        slots: Date[]
    }
}

