﻿using Owin;
using Serilog;
using System;

namespace Calndr
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var currentDirectory = AppDomain.CurrentDomain.BaseDirectory;

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.RollingFile($@"{currentDirectory}log\log-{{Date}}.txt")
                .CreateLogger();

            Log.Information("Startup");
        }
    }
}